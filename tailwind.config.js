/** @type {import('tailwindcss').Config} */
module.exports = {
    content: [
        "./assets/**/*.js",
        "./templates/**/*.{html,twig}",
        "./src/**/*.php",
    ],
    theme: {
        extend: {
            colors: {
                'app-red': '#ce0e13',
                'app-gold': '#ccc18c',
                'app-gray-100': '#f8f8f8',
                'app-gray-200': '#bababa',
                'app-gray-300': '#f7f7f7',
                'app-blue-100': '#6a7383',
                'app-blue-200': '#2a364a',
                'app-blue-300': '#121E31',
            }
        },
    },
    plugins: [],
}
