<?php

declare(strict_types=1);

namespace App\Form;

use App\Entity\Order;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TelType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class OrderType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
            ->add('name', TextType::class, [
                'label' => 'Imię',
            ])
            ->add('lastname', TextType::class, [
                'label' => 'Nazwisko',
            ])
            ->add('phone', TelType::class, [
                'label' => 'Numer telefonu',
            ])
            ->add('email', EmailType::class, [
                'label' => 'Adres e-mail',
            ])
            ->add('invoice', CheckboxType::class, [
                'label' => 'Faktura',
                'required' => false,
            ])
            ->add('company', TextType::class, [
                'label' => 'Nazwa firmy',
                'required' => false,
            ])
            ->add('taxId', TextType::class, [
                'label' => 'NIP',
                'required' => false,
            ])
            ->add('country', TextType::class, [
                'label' => 'Kraj',
                'required' => false,
            ])
            ->add('street', TextType::class, [
                'label' => 'Ulica',
                'required' => false,
            ])
            ->add('zipCode', TextType::class, [
                'label' => 'Kod pocztowy',
                'required' => false,
            ])
            ->add('city', TextType::class, [
                'label' => 'Miasto',
                'required' => false,
            ])
            ->add('terms', CheckboxType::class, [
                'label' => 'Zapoznałem się z regulaminem sklepu internetowego i akceptuję jego treść.',
                'required' => true,
                'mapped' => false,
            ])
            ->add('add', SubmitType::class, [
                'label' => 'Dodaj do koszyka',
            ]);
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'data_class' => Order::class,
        ]);
    }
}
