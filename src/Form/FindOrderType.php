<?php

declare(strict_types=1);

namespace App\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Validator\Constraints as Assert;

class FindOrderType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
            ->add('email', EmailType::class, [
                'label' => 'Adres email',
                'required' => true,
                'constraints' => [
                    new Assert\Email([
                        'message' => 'Adres {{ value }} nie jest prawidłowym adresem e-mail.',
                    ]),
                ],
            ])
            ->add('uuid', TextType::class, [
                'label' => 'Id zamówienia',
                'required' => true,
            ])
            ->add('find', SubmitType::class, [
                'label' => 'Znajdź zamówienie',
            ]);
    }
}
