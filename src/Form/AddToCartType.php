<?php

declare(strict_types=1);

namespace App\Form;

use App\Entity\OrderItem;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class AddToCartType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
            ->add('comments', TextareaType::class, [
                'label' => 'Uwagi:',
                'help' => $options['helpText'],
                'required' => false,
            ])
            ->add('clientName', TextType::class, [
                'label' => 'Imię',
                'required' => false,
            ])
            ->add('clientLastname', TextType::class, [
                'label' => 'Nazwisko',
                'required' => false,
            ])
            ->add('gift', CheckboxType::class, [
                'required' => false,
            ])
            ->add('add', SubmitType::class, [
                'label' => 'Dodaj do koszyka',
            ]);
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'data_class' => OrderItem::class,
            'helpText' => '',
        ]);
    }
}
