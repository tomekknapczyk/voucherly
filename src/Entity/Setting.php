<?php

declare(strict_types=1);

namespace App\Entity;

use App\Repository\SettingRepository;
use Doctrine\DBAL\Types\Types;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\HttpFoundation\File\File;
use Vich\UploaderBundle\Mapping\Annotation as Vich;
use Vich\UploaderBundle\Mapping\Annotation\UploadableField;

#[ORM\Entity(repositoryClass: SettingRepository::class)]
#[Vich\Uploadable]
class Setting
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column]
    private ?int $id = null;

    #[ORM\Column(length: 255)]
    private ?string $name = null;

    #[ORM\Column(length: 255, nullable: true)]
    private ?string $phone = null;

    #[ORM\Column(length: 255, nullable: true)]
    private ?string $email = null;

    #[ORM\Column(type: Types::TEXT, nullable: true)]
    private ?string $address = null;

    #[ORM\Column(length: 255, nullable: true)]
    private ?string $taxId = null;

    #[ORM\Column(length: 255, nullable: true)]
    private ?string $bankAccount = null;

    #[UploadableField(mapping: 'misc', fileNameProperty: 'fileName', size: 'fileSize')]
    private ?File $file = null;

    #[ORM\Column(type: 'string', length: 255, nullable: true)]
    private ?string $fileName = '';

    #[ORM\Column(type: 'integer', nullable: true)]
    private ?int $fileSize = 0;

    #[UploadableField(mapping: 'misc', fileNameProperty: 'faviconName', size: 'faviconSize')]
    private ?File $favicon = null;

    #[ORM\Column(type: 'string', length: 255, nullable: true)]
    private ?string $faviconName = '';

    #[ORM\Column(type: 'integer', nullable: true)]
    private ?int $faviconSize = 0;

    #[UploadableField(mapping: 'misc', fileNameProperty: 'defaultVoucherName', size: 'defaultVoucherSize')]
    private ?File $defaultVoucher = null;

    #[ORM\Column(type: 'string', length: 255, nullable: true)]
    private ?string $defaultVoucherName = '';

    #[ORM\Column(type: 'integer', nullable: true)]
    private ?int $defaultVoucherSize = 0;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getPhone(): ?string
    {
        return $this->phone;
    }

    public function setPhone(?string $phone): self
    {
        $this->phone = $phone;

        return $this;
    }

    public function getEmail(): ?string
    {
        return $this->email;
    }

    public function setEmail(?string $email): self
    {
        $this->email = $email;

        return $this;
    }

    public function getAddress(): ?string
    {
        return $this->address;
    }

    public function setAddress(?string $address): self
    {
        $this->address = $address;

        return $this;
    }

    public function getTaxId(): ?string
    {
        return $this->taxId;
    }

    public function setTaxId(?string $taxId): self
    {
        $this->taxId = $taxId;

        return $this;
    }

    public function getBankAccount(): ?string
    {
        return $this->bankAccount;
    }

    public function setBankAccount(?string $bankAccount): self
    {
        $this->bankAccount = $bankAccount;

        return $this;
    }

    public function getFile(): ?File
    {
        return $this->file;
    }

    public function setFile(File $file): void
    {
        $this->file = $file;
    }

    public function getFileName(): ?string
    {
        return $this->fileName;
    }

    public function setFileName(?string $fileName): self
    {
        $this->fileName = $fileName;

        return $this;
    }

    public function getFileSize(): ?int
    {
        return $this->fileSize;
    }

    public function setFileSize(?int $fileSize): self
    {
        $this->fileSize = $fileSize;

        return $this;
    }

    public function getFavicon(): ?File
    {
        return $this->favicon;
    }

    public function setFavicon(File $favicon): void
    {
        $this->favicon = $favicon;
    }

    public function getFaviconName(): ?string
    {
        return $this->faviconName;
    }

    public function setFaviconName(?string $faviconName): self
    {
        $this->faviconName = $faviconName;

        return $this;
    }

    public function getFaviconSize(): ?int
    {
        return $this->faviconSize;
    }

    public function setFaviconSize(?int $faviconSize): self
    {
        $this->faviconSize = $faviconSize;

        return $this;
    }

    public function getDefaultVoucher(): ?File
    {
        return $this->defaultVoucher;
    }

    public function setDefaultVoucher(File $defaultVoucher): void
    {
        $this->defaultVoucher = $defaultVoucher;
    }

    public function getDefaultVoucherName(): ?string
    {
        return $this->defaultVoucherName;
    }

    public function setDefaultVoucherName(?string $defaultVoucherName): self
    {
        $this->defaultVoucherName = $defaultVoucherName;

        return $this;
    }

    public function getDefaultVoucherSize(): ?int
    {
        return $this->defaultVoucherSize;
    }

    public function setDefaultVoucherSize(?int $defaultVoucherSize): self
    {
        $this->defaultVoucherSize = $defaultVoucherSize;

        return $this;
    }
}
