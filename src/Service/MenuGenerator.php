<?php

declare(strict_types=1);

namespace App\Service;

use App\Entity\Category;
use App\Entity\Order;
use App\Entity\Page;
use App\Entity\Setting;
use App\Repository\CategoryRepository;
use App\Repository\PageRepository;
use App\Repository\SettingRepository;
use Symfony\Contracts\Cache\CacheInterface;
use Symfony\Contracts\Cache\ItemInterface;

class MenuGenerator
{
    /**
     * @var array<Category>
     */
    private readonly array $categories;

    /**
     * @var array<Page>
     */
    private readonly array $pages;

    private readonly Setting $settings;

    public function __construct(
        private readonly CacheInterface $cache,
        CategoryRepository $categoryRepository,
        SettingRepository $settingRepository,
        PageRepository $pageRepository,
        private readonly CartService $cartService
    ) {
        $this->categories = $this->cache->get('categories', function (ItemInterface $item) use ($categoryRepository) {
            $item->expiresAfter(3600);
            return $categoryRepository->findAll();
        });

        $this->settings = $this->cache->get('settings', function (ItemInterface $item) use ($settingRepository) {
            $item->expiresAfter(3600);
            return $settingRepository->findOneBy([]);
        });

        $this->pages = $this->cache->get('pages', function (ItemInterface $item) use ($pageRepository) {
            $item->expiresAfter(3600);
            return $pageRepository->findAll();
        });
    }

    /**
     * @return array<Category>
     */
    public function categories(): array
    {
        return $this->categories;
    }

    /**
     * @return array<Page>
     */
    public function pages(): array
    {
        return $this->pages;
    }

    public function settings(): Setting
    {
        return $this->settings;
    }

    public function cart(): Order
    {
        return $this->cartService->getCurrentCart();
    }
}
