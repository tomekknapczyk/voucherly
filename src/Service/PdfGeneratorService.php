<?php

declare(strict_types=1);

namespace App\Service;

use App\Entity\OrderItem;
use Dompdf\Dompdf;
use Dompdf\Options;
use Twig\Environment;

class PdfGeneratorService
{
    public function __construct(
        private readonly Environment $twig,
        private readonly MenuGenerator $generator,
    ) {
    }

    public function generatePdf(OrderItem $voucher, bool $returnFile = false): ?string
    {
        $options = new Options();
        $options->setIsRemoteEnabled(true);
        $pdf = new Dompdf($options);

        if ($voucher->getProduct()?->getTemplate() !== null) {
            $html = $this->twig->render('pdf/template.html.twig', [
                'voucher' => $voucher,
            ]);
        } else {
            $html = $this->twig->render('pdf/default.html.twig', [
                'template' => $this->generator->settings()->getDefaultVoucherName(),
                'voucher' => $voucher,
            ]);
        }

        $pdf->loadHtml($html);
        $pdf->setPaper('A4');
        $pdf->render();

        if ($returnFile) {
            $pdf->stream();
        }

        return $pdf->output();
    }
}
