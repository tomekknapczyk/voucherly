<?php

declare(strict_types=1);

namespace App\Service;

use App\Entity\Order;
use Psr\Log\LoggerInterface;
use Symfony\Bridge\Twig\Mime\TemplatedEmail;
use Symfony\Component\Mailer\Exception\TransportExceptionInterface;
use Symfony\Component\Mailer\MailerInterface;
use Symfony\Component\Mime\Email;
use Symfony\Component\Mime\Message;

class MailService
{
    public function __construct(
        private readonly string $appEmail,
        private readonly MailerInterface $mailer,
        private readonly LoggerInterface $logger,
        private readonly PdfGeneratorService $pdfGeneratorService
    ) {
    }

    public function notifyAboutNewOrder(Order $order): void
    {
        $email = (new TemplatedEmail())
            ->from($this->appEmail)
            ->to($this->appEmail)
            ->subject('Nowe zamówienie')
            ->htmlTemplate('emails/new-order.html.twig')
            ->context([
                'order' => $order,
            ]);

        $this->send($email);
    }

    public function notifyAboutCorrectPayment(Order $order): void
    {
        if (!$order->getEmail()) {
            return;
        }

        $vouchers = [];

        foreach ($order->getItems() as $item) {
            $pdf = $this->pdfGeneratorService->generatePdf($item);

            if ($pdf) {
                $vouchers[] = [
                    'file' => $pdf,
                    'name' => 'voucher-' . $item->getUuid() . '.pdf',
                ];
            }
        }

        $email = (new TemplatedEmail())
            ->from($this->appEmail)
            ->to($order->getEmail())
            ->subject('Zamówienie zostało złożone')
            ->htmlTemplate('emails/order-paid.html.twig')
            ->context([
                'order' => $order,
            ]);

        foreach ($vouchers as $voucher) {
            $email->attach($voucher['file'], $voucher['name']);
        }

        $this->send($email);
    }

    public function testMail(string $mail): void
    {
        $email = (new Email())
            ->from($this->appEmail)
            ->to($mail)
            ->subject('Nowe zamówienie')
            ->text('TEST');

        $this->send($email);
    }

    protected function send(Message $email): void
    {
        try {
            $this->mailer->send($email);
        } catch (TransportExceptionInterface $e) {
            $this->logger->warning($e->getMessage());
        }
    }
}
