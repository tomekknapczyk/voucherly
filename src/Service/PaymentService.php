<?php

declare(strict_types=1);

namespace App\Service;

use App\Entity\Order;
use InvalidArgumentException;
use LogicException;
use Symfony\Component\HttpClient\Response\TraceableResponse;
use Symfony\Contracts\HttpClient\Exception\ClientExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\RedirectionExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\ServerExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\TransportExceptionInterface;
use Symfony\Contracts\HttpClient\HttpClientInterface;

class PaymentService
{
    public function __construct(
        private readonly string $paymentUrl,
        private readonly string $merchantId,
        private readonly string $serviceKey,
        private readonly string $serviceId,
        private readonly string $title,
        private readonly string $host,
        private readonly HttpClientInterface $httpClient,
    ) {
    }

    /**
     * @throws TransportExceptionInterface
     * @throws ServerExceptionInterface
     * @throws RedirectionExceptionInterface
     * @throws ClientExceptionInterface
     */
    public function createTransaction(Order $order): string
    {
        $hashMethod = 'sha256';

        $fields = [
            'merchantId' => $this->merchantId,
            'serviceId' => $this->serviceId,
            'amount' => $order->getValue(),
            'currency' => 'PLN',
            'orderId' => (string) $order->getUuid(),
            'orderDescription' => $this->title,
            'customerFirstName' => $order->getName(),
            'customerLastName' => $order->getLastName(),
            'customerEmail' => $order->getEmail(),
            'customerPhone' => $order->getPhone(),
            'urlReturn' => 'https://' . $this->host . '/payment-return',
        ];

        $fields['signature'] = $this->createSignature($fields, $this->serviceKey, $hashMethod) . ';' . $hashMethod;

        /** @var TraceableResponse $response */
        $response = $this->httpClient->request('POST', $this->paymentUrl, [
            'body' => $fields,
        ]);

        $stream = $response->toStream();
        $meta = stream_get_meta_data($stream);
        return $meta['wrapper_data']->getResponse()->getInfo('url');
    }

    public function verifySignature(string $content, ?string $signature): void
    {
        if ($signature === null) {
            throw new InvalidArgumentException('Brak sygnatury');
        }
        $signatureFields = [];

        foreach (explode(';', $signature) as $field) {
            [$name, $value] = explode('=', $field);
            $signatureFields[$name] = $value;
        }

        if (!array_key_exists('signature', $signatureFields) || !array_key_exists('alg', $signatureFields) || hash($signatureFields['alg'], $content . $this->serviceKey) !== $signatureFields['signature']) {
            throw new LogicException('Błędna sygnatura');
        }
    }

    /**
     * @param array<string, mixed> $parameters
     */
    public function verifyPaymentStatus(array $parameters, string $status): bool
    {
        return $parameters['payment']['status'] === $status;
    }

    /**
     * @param array<string, int|string|null> $orderData
     */
    private function createSignature(array $orderData, string $serviceKey, string $hashMethod): string
    {
        $data = $this->prepareData($orderData);

        return hash($hashMethod, $data . $serviceKey);
    }

    /**
     * @param array<string, int|string|null> $data
     */
    private function prepareData(array $data): string
    {
        ksort($data);
        $hashData = [];
        foreach ($data as $key => $value) {
            $hashData[] = $key . '=' . $value;
        }

        return implode('&', $hashData);
    }
}
