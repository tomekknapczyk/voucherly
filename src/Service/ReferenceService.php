<?php

declare(strict_types=1);

namespace App\Service;

use App\Repository\OrderItemRepository;
use App\Repository\OrderRepository;
use Exception;

class ReferenceService
{
    public function __construct(
        private readonly OrderItemRepository $orderItemRepository,
        private readonly OrderRepository $orderRepository,
    ) {
    }

    /**
     * @throws Exception
     */
    public function generateUniqReferenceForOrder(): string
    {
        $reference = bin2hex(random_bytes(4));

        $referenceExists = $this->orderRepository->findOneBy([
            'uuid' => $reference,
        ]);

        if (!$referenceExists) {
            return $reference;
        }

        return $this->generateUniqReferenceForOrder();
    }

    /**
     * @throws Exception
     */
    public function generateUniqReferenceForOrderItem(): string
    {
        $reference = bin2hex(random_bytes(4));

        $referenceExists = $this->orderItemRepository->findOneBy([
            'uuid' => $reference,
        ]);

        if (!$referenceExists) {
            return $reference;
        }

        return $this->generateUniqReferenceForOrderItem();
    }
}
