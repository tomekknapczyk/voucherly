<?php

declare(strict_types=1);

namespace App\Service;

use App\Entity\Order;
use App\Entity\OrderItem;
use App\Event\OrderPaidEvent;
use App\Event\OrderPlacedEvent;
use App\Repository\OrderRepository;
use App\Storage\CartSessionStorage;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;

class CartService
{
    public function __construct(
        private readonly CartSessionStorage $cartStorage,
        private readonly OrderRepository $orderRepository,
        private readonly EventDispatcherInterface $dispatcher,
        private readonly ReferenceService $referenceService,
    ) {
    }

    public function save(Order $cart): void
    {
        $this->orderRepository->add($cart, true);
        $this->cartStorage->setCart($cart);
    }

    public function removeItem(OrderItem $orderItem): void
    {
        $cart = $this->getCurrentCart();
        $cart->removeItem($orderItem);
        $this->orderRepository->add($cart, true);
    }

    public function getCurrentCart(): Order
    {
        $cart = $this->cartStorage->getCart();

        if (!$cart) {
            $cart = new Order();
        }

        return $cart;
    }

    public function storeCart(bool $isPaid = false): Order
    {
        $cart = $this->getCurrentCart();
        $total = 0;

        foreach ($cart->getItems() as $item) {
            $product = $item->getProduct();

            if ($product) {
                $item->setPrice($product->getPrice());
                $item->setName($product->getName());
                $item->setValidThru($product->validThru());

                if (!$item->isGift()) {
                    $item->setClientName($cart->getName());
                    $item->setClientLastName($cart->getLastName());
                    $item->setPhone($cart->getPhone());
                    $item->setEmail($cart->getEmail());
                }
                $total += $product->getPrice();
            } else {
                $cart->removeItem($item);
            }
        }

        $cart->setValue($total);

        if ($isPaid) {
            $cart->setStatus(Order::STATUS_PAID);
        } else {
            $cart->setStatus(Order::STATUS_NEW);
        }

        $cart->setUuid($this->referenceService->generateUniqReferenceForOrder());

        $this->orderRepository->add($cart, true);

        if ($isPaid) {
            $event = new OrderPaidEvent($cart);
            $this->dispatcher->dispatch($event, OrderPaidEvent::NAME);
        } else {
            $event = new OrderPlacedEvent($cart);
            $this->dispatcher->dispatch($event, OrderPlacedEvent::NAME);
        }

        return $cart;
    }
}
