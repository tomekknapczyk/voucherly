<?php

declare(strict_types=1);

namespace App\Twig\Extension;

use Endroid\QrCode\Builder\BuilderRegistryInterface;
use Twig\Extension\AbstractExtension;
use Twig\TwigFunction;

class QrCodeExtension extends AbstractExtension
{
    private BuilderRegistryInterface $builderRegistry;

    public function __construct(BuilderRegistryInterface $builderRegistry)
    {
        $this->builderRegistry = $builderRegistry;
    }

    public function getFunctions(): array
    {
        return [
            new TwigFunction('user_qr_code', [$this, 'userQrCode']),
        ];
    }

    public function userQrCode(string $uuid): string
    {
        $builder = $this->builderRegistry->getBuilder('default');

        return $builder
            ->data($uuid)
            ->build()
            ->getDataUri();
    }
}
