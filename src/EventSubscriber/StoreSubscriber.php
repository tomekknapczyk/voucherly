<?php

declare(strict_types=1);

namespace App\EventSubscriber;

use App\Event\OrderPaidEvent;
use App\Event\OrderPlacedEvent;
use App\Service\MailService;
use JetBrains\PhpStorm\ArrayShape;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;

class StoreSubscriber implements EventSubscriberInterface
{
    public function __construct(private readonly MailService $mailService)
    {
    }

    #[ArrayShape([
        OrderPlacedEvent::NAME => 'string',
        OrderPaidEvent::NAME => 'string',
    ])]
    public static function getSubscribedEvents(): array
    {
        return [
            OrderPlacedEvent::NAME => 'onOrderPlaced',
            OrderPaidEvent::NAME => 'onOrderPaid',
        ];
    }

    public function onOrderPlaced(OrderPlacedEvent $event): void
    {
        $this->mailService->notifyAboutNewOrder($event->getOrder());
    }

    public function onOrderPaid(OrderPaidEvent $event): void
    {
        $this->mailService->notifyAboutCorrectPayment($event->getOrder());
    }
}
