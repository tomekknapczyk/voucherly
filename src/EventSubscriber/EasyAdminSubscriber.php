<?php

declare(strict_types=1);

namespace App\EventSubscriber;

use App\Entity\Category;
use App\Entity\Page;
use App\Entity\Product;
use App\Entity\Setting;
use EasyCorp\Bundle\EasyAdminBundle\Event\AbstractLifecycleEvent;
use EasyCorp\Bundle\EasyAdminBundle\Event\AfterEntityDeletedEvent;
use EasyCorp\Bundle\EasyAdminBundle\Event\AfterEntityPersistedEvent;
use EasyCorp\Bundle\EasyAdminBundle\Event\AfterEntityUpdatedEvent;
use EasyCorp\Bundle\EasyAdminBundle\Event\BeforeEntityDeletedEvent;
use JetBrains\PhpStorm\ArrayShape;
use Psr\Cache\InvalidArgumentException;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Contracts\Cache\CacheInterface;

class EasyAdminSubscriber implements EventSubscriberInterface
{
    public function __construct(private readonly CacheInterface $cache)
    {
    }

    #[ArrayShape([
        AfterEntityDeletedEvent::class => 'string[]',
        AfterEntityPersistedEvent::class => 'string[]',
        AfterEntityUpdatedEvent::class => 'string[]',
        BeforeEntityDeletedEvent::class => 'string[]',
    ])]
    public static function getSubscribedEvents(): array
    {
        return [
            AfterEntityDeletedEvent::class => ['clearCache'],
            AfterEntityPersistedEvent::class => ['clearCache'],
            AfterEntityUpdatedEvent::class => ['clearCache'],
            BeforeEntityDeletedEvent::class => ['dissociateRelations'],
        ];
    }

    /**
     * @throws InvalidArgumentException
     */
    public function clearCache(AbstractLifecycleEvent $event): void
    {
        $entity = $event->getEntityInstance();

        if ($entity instanceof Category) {
            $this->cache->delete('categories');
        }

        if ($entity instanceof Page) {
            $this->cache->delete('pages');
        }
        if ($entity instanceof Setting) {
            $this->cache->delete('settings');
        }
    }

    public function dissociateRelations(AbstractLifecycleEvent $event): void
    {
        $entity = $event->getEntityInstance();

        if ($entity instanceof Product) {
            foreach ($entity->getOrderItems() as $orderItem) {
                $entity->removeOrderItem($orderItem);
            }
        }
    }
}
