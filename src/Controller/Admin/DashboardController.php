<?php

declare(strict_types=1);

namespace App\Controller\Admin;

use App\Entity\Category;
use App\Entity\Order;
use App\Entity\OrderItem;
use App\Entity\Page;
use App\Entity\Product;
use App\Entity\Setting;
use App\Entity\Template;
use EasyCorp\Bundle\EasyAdminBundle\Config\Dashboard;
use EasyCorp\Bundle\EasyAdminBundle\Config\MenuItem;
use EasyCorp\Bundle\EasyAdminBundle\Controller\AbstractDashboardController;
use EasyCorp\Bundle\EasyAdminBundle\Router\AdminUrlGenerator;
use Psr\Container\ContainerExceptionInterface;
use Psr\Container\NotFoundExceptionInterface;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class DashboardController extends AbstractDashboardController
{
    /**
     * @throws ContainerExceptionInterface
     * @throws NotFoundExceptionInterface
     */
    #[Route('/admin', name: 'admin')]
    public function index(): Response
    {
        $adminUrlGenerator = $this->container->get(AdminUrlGenerator::class);
        return $this->redirect($adminUrlGenerator->setController(OrderCrudController::class)->generateUrl());
    }

    public function configureDashboard(): Dashboard
    {
        $appName = $this->getParameter('app.name') ?? 'Voucherly';
        return Dashboard::new()
            ->setTitle(strval($appName));
    }

    public function configureMenuItems(): iterable
    {
        yield MenuItem::linkToCrud('Zamówienia', 'fas fa-box', Order::class);
        yield MenuItem::linkToCrud('Vouchery', 'fas fa-ticket', OrderItem::class);
        yield MenuItem::linkToCrud('Kategorie', 'fas fa-archive', Category::class);
        yield MenuItem::linkToCrud('Produkty', 'fas fa-gift', Product::class);
        yield MenuItem::linkToCrud('Szablony voucherów', 'fas fa-image', Template::class);
        yield MenuItem::linkToCrud('Strony', 'fas fa-file', Page::class);
        yield MenuItem::linkToCrud('Ustawienia', 'fas fa-cogs', Setting::class);
    }
}
