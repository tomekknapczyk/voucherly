<?php

declare(strict_types=1);

namespace App\Controller\Admin;

use App\Entity\Category;
use App\Field\CKEditorField;
use EasyCorp\Bundle\EasyAdminBundle\Config\Actions;
use EasyCorp\Bundle\EasyAdminBundle\Config\Crud;
use EasyCorp\Bundle\EasyAdminBundle\Controller\AbstractCrudController;
use EasyCorp\Bundle\EasyAdminBundle\Field\ImageField;
use EasyCorp\Bundle\EasyAdminBundle\Field\TextField;

class CategoryCrudController extends AbstractCrudController
{
    public static function getEntityFqcn(): string
    {
        return Category::class;
    }

    public function configureCrud(Crud $crud): Crud
    {
        return $crud
            ->setEntityLabelInSingular('Kategoria')
            ->setEntityLabelInPlural('Kategorie')
            ->addFormTheme('@FOSCKEditor/Form/ckeditor_widget.html.twig');
    }

    public function configureFields(string $pageName): iterable
    {
        yield TextField::new('name', 'Tytuł kategorii');
        yield ImageField::new('iconName', 'Ikona')->onlyOnForms()->setUploadDir('public/images/categories');
        yield ImageField::new('iconName', 'Ikona')->onlyOnDetail()->setTemplatePath('admin/category-thumbnail.html.twig');
        yield CKEditorField::new('description', 'Opis kategorii')->onlyOnForms();
        yield TextField::new('description', 'Opis kategorii')
            ->onlyOnDetail()
            ->setTemplatePath('admin/description.html.twig');
        yield ImageField::new('fileName', 'Obraz')->onlyOnForms()->setUploadDir('public/images/categories');
        yield ImageField::new('fileName', 'Obraz')->onlyOnDetail()->setTemplatePath('admin/category-thumbnail.html.twig');
    }

    public function configureActions(Actions $actions): Actions
    {
        return $actions->add(Crud::PAGE_INDEX, 'detail');
    }
}
