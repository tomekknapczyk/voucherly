<?php

declare(strict_types=1);

namespace App\Controller\Admin;

use App\Entity\Setting;
use EasyCorp\Bundle\EasyAdminBundle\Config\Actions;
use EasyCorp\Bundle\EasyAdminBundle\Config\Crud;
use EasyCorp\Bundle\EasyAdminBundle\Controller\AbstractCrudController;
use EasyCorp\Bundle\EasyAdminBundle\Field\EmailField;
use EasyCorp\Bundle\EasyAdminBundle\Field\FormField;
use EasyCorp\Bundle\EasyAdminBundle\Field\ImageField;
use EasyCorp\Bundle\EasyAdminBundle\Field\TelephoneField;
use EasyCorp\Bundle\EasyAdminBundle\Field\TextareaField;
use EasyCorp\Bundle\EasyAdminBundle\Field\TextField;

class SettingCrudController extends AbstractCrudController
{
    public static function getEntityFqcn(): string
    {
        return Setting::class;
    }

    public function configureCrud(Crud $crud): Crud
    {
        return $crud
            ->setEntityLabelInSingular('Ustawienia')
            ->setEntityLabelInPlural('Ustawienia')
            ->addFormTheme('@FOSCKEditor/Form/ckeditor_widget.html.twig');
    }

    public function configureFields(string $pageName): iterable
    {
        return [
            FormField::addTab('Podstawowe informacje'),
            TextField::new('name', 'Nazwa sklepu'),
            ImageField::new('fileName', 'Logo')
                ->onlyOnForms()
                ->setUploadDir('public/images/misc'),
            ImageField::new('faviconName', 'Favicon')
                ->onlyOnForms()
                ->setUploadDir('public/images/misc'),
            ImageField::new('defaultVoucherName', 'Standardowy szablon')
                ->onlyOnForms()
                ->setUploadDir('public/images/misc'),
            ImageField::new('fileName', 'Logo')
                ->onlyOnDetail()
                ->setTemplatePath('admin/misc-thumbnail.html.twig'),
            ImageField::new('faviconName', 'Favicon')
                ->onlyOnDetail()
                ->setTemplatePath('admin/misc-thumbnail.html.twig'),
            ImageField::new('defaultVoucherName', 'Standardowy szablon')
                ->onlyOnDetail()
                ->setTemplatePath('admin/misc-thumbnail.html.twig'),
            FormField::addTab('Dane kontaktowe'),
            TextareaField::new('address', 'Adres')->setNumOfRows(5),
            TelephoneField::new('phone', 'Numer telefonu'),
            EmailField::new('email', 'Adres email'),
            TextField::new('taxId', 'NIP'),
            TextField::new('bankAccount', 'Numer konta bankowego'),
        ];
    }

    public function configureActions(Actions $actions): Actions
    {
        return $actions->remove(Crud::PAGE_INDEX, 'delete')
            ->remove(Crud::PAGE_INDEX, 'new')
            ->remove(Crud::PAGE_DETAIL, 'delete')
            ->add(Crud::PAGE_INDEX, 'detail');
    }
}
