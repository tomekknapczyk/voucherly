<?php

declare(strict_types=1);

namespace App\Controller\Admin;

use App\Entity\Order;
use App\Entity\OrderItem;
use Doctrine\ORM\QueryBuilder;
use EasyCorp\Bundle\EasyAdminBundle\Collection\FieldCollection;
use EasyCorp\Bundle\EasyAdminBundle\Collection\FilterCollection;
use EasyCorp\Bundle\EasyAdminBundle\Config\Action;
use EasyCorp\Bundle\EasyAdminBundle\Config\Actions;
use EasyCorp\Bundle\EasyAdminBundle\Config\Crud;
use EasyCorp\Bundle\EasyAdminBundle\Controller\AbstractCrudController;
use EasyCorp\Bundle\EasyAdminBundle\Dto\EntityDto;
use EasyCorp\Bundle\EasyAdminBundle\Dto\SearchDto;
use EasyCorp\Bundle\EasyAdminBundle\Field\AssociationField;
use EasyCorp\Bundle\EasyAdminBundle\Field\BooleanField;
use EasyCorp\Bundle\EasyAdminBundle\Field\DateField;
use EasyCorp\Bundle\EasyAdminBundle\Field\FormField;
use EasyCorp\Bundle\EasyAdminBundle\Field\MoneyField;
use EasyCorp\Bundle\EasyAdminBundle\Field\TextField;
use EasyCorp\Bundle\EasyAdminBundle\Orm\EntityRepository;

class OrderItemCrudController extends AbstractCrudController
{
    public static function getEntityFqcn(): string
    {
        return OrderItem::class;
    }

    public function configureCrud(Crud $crud): Crud
    {
        return $crud
            ->setEntityLabelInSingular('Voucher')
            ->setEntityLabelInPlural('Vouchery');
    }

    public function createIndexQueryBuilder(SearchDto $searchDto, EntityDto $entityDto, FieldCollection $fields, FilterCollection $filters): QueryBuilder
    {
        parent::createIndexQueryBuilder($searchDto, $entityDto, $fields, $filters);

        $response = $this->container->get(EntityRepository::class)->createQueryBuilder($searchDto, $entityDto, $fields, $filters);
        $response->join('entity.orderRef', 'orderRef')
            ->andWhere('orderRef.status = :paid')
            ->setParameter('paid', Order::STATUS_PAID)
            ->andWhere('orderRef.deleted = 0');

        return $response;
    }

    public function configureFields(string $pageName): iterable
    {
        return [
            FormField::addTab('Podstawowe informacje'),
            TextField::new('uuid', 'ID')->hideOnForm(),
            BooleanField::new('used', 'Wykorzystany')->onlyOnIndex()->setDisabled(),
            BooleanField::new('used', 'Wykorzystany')->onlyOnForms(),
            BooleanField::new('gift', 'Prezent')->hideOnForm()->setDisabled(),
            TextField::new('clientName', 'Imię'),
            TextField::new('clientLastname', 'Nazwisko'),
            TextField::new('phone', 'Numer telefonu'),
            TextField::new('email', 'Adres e-mail'),
            TextField::new('name', 'Produkt')->hideOnForm(),
            MoneyField::new('price', 'Wartość vouchera')->setCurrency('PLN')->onlyOnDetail(),
            DateField::new('updatedAt', 'Data aktualizacji')
                ->onlyOnDetail()
                ->setFormat('Y-M-d H:mm:ss'),
            TextField::new('comments', 'Uwagi')
                ->onlyOnDetail(),
            AssociationField::new('orderRef', 'Zamówienie')
                ->onlyOnDetail()
                ->setTemplatePath('admin/order.html.twig'),
            DateField::new('validThru', 'Data ważności')
                ->hideOnForm()
                ->setTemplatePath('admin/valid-thru.html.twig'),
            DateField::new('usedAt', 'Data wykorzystania')
                ->setFormat('Y-M-d'),
        ];
    }

    public function configureActions(Actions $actions): Actions
    {
        $generatePdf = Action::new('generatePdf', 'PDF', 'fa fa-file-download')
            ->linkToRoute('generatePdf', function (OrderItem $voucher): array {
                return [
                    'id' => $voucher->getId(),
                ];
            });

        return $actions->remove(Crud::PAGE_INDEX, 'delete')
            ->remove(Crud::PAGE_INDEX, 'new')
            ->remove(Crud::PAGE_DETAIL, 'delete')
            ->add(Crud::PAGE_INDEX, 'detail')
            ->add(Crud::PAGE_INDEX, $generatePdf)
            ->add(Crud::PAGE_DETAIL, $generatePdf);
    }
}
