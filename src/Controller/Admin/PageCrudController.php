<?php

declare(strict_types=1);

namespace App\Controller\Admin;

use App\Entity\Page;
use App\Field\CKEditorField;
use EasyCorp\Bundle\EasyAdminBundle\Config\Crud;
use EasyCorp\Bundle\EasyAdminBundle\Controller\AbstractCrudController;
use EasyCorp\Bundle\EasyAdminBundle\Field\DateField;
use EasyCorp\Bundle\EasyAdminBundle\Field\SlugField;
use EasyCorp\Bundle\EasyAdminBundle\Field\TextField;

class PageCrudController extends AbstractCrudController
{
    public static function getEntityFqcn(): string
    {
        return Page::class;
    }

    public function configureCrud(Crud $crud): Crud
    {
        return $crud
            ->setEntityLabelInSingular('Strona')
            ->setEntityLabelInPlural('Strony')
            ->addFormTheme('@FOSCKEditor/Form/ckeditor_widget.html.twig');
    }

    public function configureFields(string $pageName): iterable
    {
        yield TextField::new('title', 'Tytuł strony');
        yield SlugField::new('slug', 'Alias')->setTargetFieldName('title')->setUnlockConfirmationMessage(
            'Wysoce zalecane jest używanie automatycznych aliasów, ale zawsze możesz je dostosować'
        );
        yield CKEditorField::new('content', 'Treść')->hideOnIndex();
        yield DateField::new('createdAt', 'Data utworzenia')->onlyOnIndex()->setFormat('Y-M-d H:mm:ss');
        yield DateField::new('updatedAt', 'Data aktualizacji')->onlyOnIndex()->setFormat('Y-M-d H:mm:ss');
    }
}
