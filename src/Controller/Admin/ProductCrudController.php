<?php

declare(strict_types=1);

namespace App\Controller\Admin;

use App\Entity\Product;
use App\Field\CKEditorField;
use App\Form\ImageType;
use EasyCorp\Bundle\EasyAdminBundle\Config\Actions;
use EasyCorp\Bundle\EasyAdminBundle\Config\Crud;
use EasyCorp\Bundle\EasyAdminBundle\Controller\AbstractCrudController;
use EasyCorp\Bundle\EasyAdminBundle\Field\AssociationField;
use EasyCorp\Bundle\EasyAdminBundle\Field\BooleanField;
use EasyCorp\Bundle\EasyAdminBundle\Field\CollectionField;
use EasyCorp\Bundle\EasyAdminBundle\Field\DateField;
use EasyCorp\Bundle\EasyAdminBundle\Field\FormField;
use EasyCorp\Bundle\EasyAdminBundle\Field\ImageField;
use EasyCorp\Bundle\EasyAdminBundle\Field\IntegerField;
use EasyCorp\Bundle\EasyAdminBundle\Field\MoneyField;
use EasyCorp\Bundle\EasyAdminBundle\Field\TextField;

class ProductCrudController extends AbstractCrudController
{
    public static function getEntityFqcn(): string
    {
        return Product::class;
    }

    public function configureCrud(Crud $crud): Crud
    {
        return $crud
            ->setEntityLabelInSingular('Produkt')
            ->setEntityLabelInPlural('Produkty')
            ->addFormTheme('@FOSCKEditor/Form/ckeditor_widget.html.twig');
    }

    public function configureFields(string $pageName): iterable
    {
        return [
            FormField::addTab('Informacje o produkcie'),
            TextField::new('name', 'Nazwa produktu'),
            BooleanField::new('active', 'Dostępny'),
            ImageField::new('fileName', 'Zdjęcie główne')
                ->onlyOnForms()
                ->setUploadDir('public/images/products'),
            ImageField::new('fileName', 'Zdjęcie główne')
                ->onlyOnDetail()
                ->setTemplatePath('admin/product-thumbnail.html.twig'),
            AssociationField::new('category', 'Kategoria')
                ->onlyOnForms(),
            AssociationField::new('template', 'Szablon vouchera')
                ->onlyOnForms(),
            MoneyField::new('price', 'Cena')->setCurrency('PLN'),
            BooleanField::new('availableAsGift', 'Możliwość zakupu jako prezent')
                ->onlyOnForms(),
            BooleanField::new('availableAsGift', 'Możliwość zakupu jako prezent')
                ->onlyOnForms(),
            IntegerField::new('validity', 'Okres ważności')
                ->setHelp('Przez ile dni od chwili zakupu, voucher będzie ważny.<br>0 - bezterminowo')
                ->hideOnIndex(),
            FormField::addTab('Opis'),
            TextField::new('place', 'Miejsce realizacji')->hideOnIndex(),
            TextField::new('time', 'Termin realizacji lotu')->hideOnIndex(),
            TextField::new('peoples', 'Liczba osób')->hideOnIndex(),
            TextField::new('duration', 'Czas lotu')->hideOnIndex(),
            TextField::new('weight', 'Waga pasażera')->hideOnIndex(),
            TextField::new('helpText', 'Treść podpowiedzi w oknie uwag')->hideOnIndex(),
            CKEditorField::new('shortDescription', 'Krótki opis')->onlyOnForms(),
            TextField::new('shortDescription', 'Krótki opis')
                ->onlyOnDetail()
                ->setTemplatePath('admin/description.html.twig'),
            CKEditorField::new('description', 'Opis')->onlyOnForms(),
            TextField::new('description', 'Opis')
                ->onlyOnDetail()
                ->setTemplatePath('admin/description.html.twig'),
            DateField::new('createdAt', 'Data utworzenia')
                ->onlyOnIndex()
                ->setFormat('Y-M-d H:mm:ss'),
            DateField::new('updatedAt', 'Data aktualizacji')
                ->onlyOnIndex()
                ->setFormat('Y-M-d H:mm:ss'),
            FormField::addTab('Galeria'),
            CollectionField::new('images', 'Galeria')
                ->setEntryType(ImageType::class)
                ->onlyOnForms(),
            CollectionField::new('images', 'Galeria')
                ->onlyOnDetail()
                ->setTemplatePath('admin/product-images.html.twig'),
        ];
    }

    public function configureActions(Actions $actions): Actions
    {
        return $actions->add(Crud::PAGE_INDEX, 'detail');
    }
}
