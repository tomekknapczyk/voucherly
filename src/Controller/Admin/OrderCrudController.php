<?php

declare(strict_types=1);

namespace App\Controller\Admin;

use App\Entity\Order;
use App\Entity\OrderItem;
use App\Repository\OrderRepository;
use Doctrine\ORM\QueryBuilder;
use EasyCorp\Bundle\EasyAdminBundle\Collection\FieldCollection;
use EasyCorp\Bundle\EasyAdminBundle\Collection\FilterCollection;
use EasyCorp\Bundle\EasyAdminBundle\Config\Action;
use EasyCorp\Bundle\EasyAdminBundle\Config\Actions;
use EasyCorp\Bundle\EasyAdminBundle\Config\Crud;
use EasyCorp\Bundle\EasyAdminBundle\Controller\AbstractCrudController;
use EasyCorp\Bundle\EasyAdminBundle\Dto\EntityDto;
use EasyCorp\Bundle\EasyAdminBundle\Dto\SearchDto;
use EasyCorp\Bundle\EasyAdminBundle\Field\BooleanField;
use EasyCorp\Bundle\EasyAdminBundle\Field\CollectionField;
use EasyCorp\Bundle\EasyAdminBundle\Field\DateField;
use EasyCorp\Bundle\EasyAdminBundle\Field\FormField;
use EasyCorp\Bundle\EasyAdminBundle\Field\MoneyField;
use EasyCorp\Bundle\EasyAdminBundle\Field\TextField;
use EasyCorp\Bundle\EasyAdminBundle\Orm\EntityRepository;
use EasyCorp\Bundle\EasyAdminBundle\Router\AdminUrlGenerator;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\Routing\Annotation\Route;

class OrderCrudController extends AbstractCrudController
{
    public static function getEntityFqcn(): string
    {
        return Order::class;
    }

    public function configureCrud(Crud $crud): Crud
    {
        return $crud
            ->setEntityLabelInSingular('Zamówienie')
            ->setEntityLabelInPlural('Zamówienia');
    }

    public function createIndexQueryBuilder(SearchDto $searchDto, EntityDto $entityDto, FieldCollection $fields, FilterCollection $filters): QueryBuilder
    {
        parent::createIndexQueryBuilder($searchDto, $entityDto, $fields, $filters);

        $response = $this->container->get(EntityRepository::class)->createQueryBuilder($searchDto, $entityDto, $fields, $filters);
        $response->andWhere("entity.status NOT IN ('" . Order::STATUS_CART . "')")
            ->andWhere('entity.deleted = 0');

        return $response;
    }

    public function configureFields(string $pageName): iterable
    {
        return [
            FormField::addTab('Podstawowe informacje')->hideOnForm(),
            TextField::new('uuid', 'ID')->hideOnForm(),
            TextField::new('status', 'Status')->hideOnForm()->setTemplatePath('admin/order-status.html.twig'),
            TextField::new('name', 'Imię')->hideOnForm(),
            TextField::new('lastname', 'Nazwisko')->hideOnForm(),
            TextField::new('phone', 'Numer telefonu')->hideOnForm(),
            TextField::new('email', 'Adres e-mail')->hideOnForm(),
            MoneyField::new('value', 'Wartość zamówienia')->setCurrency('PLN')->hideOnForm(),
            BooleanField::new('invoice', 'Faktura')->hideOnForm()->setDisabled(),
            DateField::new('createdAt', 'Data utworzenia')
                ->hideOnForm()
                ->setFormat('Y-M-d H:mm:ss'),
            DateField::new('updatedAt', 'Data aktualizacji')
                ->hideOnForm()
                ->setFormat('Y-M-d H:mm:ss'),
            FormField::addTab('Dane do faktury')->onlyOnDetail(),
            TextField::new('company', 'Nazwa firmy')->onlyOnDetail(),
            TextField::new('taxId', 'NIP')->onlyOnDetail(),
            TextField::new('country', 'Kraj')->onlyOnDetail(),
            TextField::new('city', 'Miasto')->onlyOnDetail(),
            TextField::new('zipCode', 'Kod pocztowy')->onlyOnDetail(),
            TextField::new('street', 'Ulica')->onlyOnDetail(),
            FormField::addTab('Vouchery')->onlyOnDetail(),
            CollectionField::new('items', 'Vouchery')
                ->setEntryType(OrderItem::class)
                ->onlyOnDetail()
                ->setTemplatePath('admin/voucher.html.twig'),
        ];
    }

    public function configureActions(Actions $actions): Actions
    {
        $softDelete = Action::new('softDelete', 'Usuń', 'fa fa-trash')
            ->linkToRoute('softDeleteOrder', function (Order $order): array {
                return [
                    'id' => $order->getId(),
                ];
            });

        return $actions->remove(Crud::PAGE_INDEX, 'delete')
            ->remove(Crud::PAGE_INDEX, 'new')
            ->remove(Crud::PAGE_INDEX, 'edit')
            ->remove(Crud::PAGE_DETAIL, 'delete')
            ->remove(Crud::PAGE_DETAIL, 'edit')
            ->add(Crud::PAGE_INDEX, 'detail')
            ->add(Crud::PAGE_DETAIL, $softDelete);
    }

    #[Route(path: 'admin/soft-delete-order/{id}', name: 'softDeleteOrder')]
    public function softDeleteOrder(
        Order $order,
        OrderRepository $orderRepository,
        AdminUrlGenerator $adminUrlGenerator,
    ): RedirectResponse {
        $order->setDeleted(true);
        $orderRepository->add($order, true);

        return $this->redirect($adminUrlGenerator->setController(__CLASS__)->generateUrl());
    }
}
