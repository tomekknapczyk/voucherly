<?php

declare(strict_types=1);

namespace App\Controller\Front;

use App\Entity\Category;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class CategoryController extends AbstractController
{
    #[Route(path: '/category/{id}', name: 'category')]
    public function category(Category $category): Response
    {
        return $this->render('front/category.html.twig', [
            'category' => $category,
        ]);
    }
}
