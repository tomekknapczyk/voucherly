<?php

declare(strict_types=1);

namespace App\Controller\Front;

use App\Entity\OrderItem;
use App\Service\PdfGeneratorService;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class PdfController extends AbstractController
{
    #[Route(path: 'admin/generate-pdf/{id}', name: 'generatePdf')]
    public function generatePdf(OrderItem $orderItem, PdfGeneratorService $pdfGeneratorService): Response
    {
        $pdfGeneratorService->generatePdf($orderItem, true);

        return new JsonResponse([
            'status' => 'ok',
        ], Response::HTTP_OK);
    }
}
