<?php

declare(strict_types=1);

namespace App\Controller\Front;

use App\Entity\Page;
use App\Service\MailService;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class HomeController extends AbstractController
{
    #[Route(path: '/', name: 'home')]
    public function home(): Response
    {
        return $this->render('front/home.html.twig');
    }

    #[Route(path: '/contact', name: 'contact')]
    public function contact(): Response
    {
        return $this->render('front/contact.html.twig');
    }

    #[Route(path: '/page/{slug}', name: 'static_page')]
    public function staticPage(Page $page): Response
    {
        return $this->render('front/page.html.twig', [
            'page' => $page,
        ]);
    }

    #[Route(path: '/mail-test/{mail?}', name: 'mailTest')]
    public function mailTest(Request $request, MailService $mailService): Response
    {
        $mail = $request->get('mail') ?: 'mafisz@gmail.com';
        $mailService->testMail($mail);

        return $this->render('front/home.html.twig');
    }
}
