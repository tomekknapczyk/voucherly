<?php

declare(strict_types=1);

namespace App\Controller\Front;

use App\Entity\OrderItem;
use App\Form\OrderType;
use App\Service\CartService;
use App\Service\PaymentService;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Validator\Validator\ValidatorInterface;

class CartController extends AbstractController
{
    #[Route(path: '/cart', name: 'cart')]
    public function product(Request $request, CartService $cartService, ValidatorInterface $validator, PaymentService $paymentService): Response
    {
        $cart = $cartService->getCurrentCart();

        if ($cart->getItems()->count() === 0) {
            return $this->redirectToRoute('home');
        }

        $form = $this->createForm(OrderType::class, $cart);

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $item = $form->getData();
            $valid = true;

            if (!$form->get('terms')->getData()) {
                $this->addFlash('danger', 'Akceptacja regulaminu jest wymagana');
                $valid = false;
            }

            if ($item->isInvoice()) {
                $errors = $validator->validate($item, null, ['invoice']);
                if (count($errors) > 0) {
                    $this->addFlash('danger', 'Uzupełnij wszystkie wymagane pola');
                    $valid = false;
                }
            }

            if ($valid) {
                $user = $this->getUser();

                if ($user) {
                    $cartService->storeCart(true);

                    return $this->redirectToRoute('home');
                }

                $cart = $cartService->storeCart();
                $paymentLink = $paymentService->createTransaction($cart);

                return $this->redirect($paymentLink);
            }
        }

        return $this->render('front/cart.html.twig', [
            'cart' => $cart,
            'form' => $form->createView(),
        ]);
    }

    #[Route(path: '/remove-item/{id}', name: 'removeItem')]
    public function removeItem(OrderItem $orderItem, CartService $cartService): Response
    {
        $cartService->removeItem($orderItem);

        return $this->redirectToRoute('cart');
    }
}
