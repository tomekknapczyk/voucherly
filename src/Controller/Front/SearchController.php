<?php

declare(strict_types=1);

namespace App\Controller\Front;

use App\Form\FindOrderType;
use App\Repository\OrderRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class SearchController extends AbstractController
{
    #[Route(path: '/find-order', name: 'findOrder')]
    public function findOrder(Request $request, OrderRepository $orderRepository): Response
    {
        $order = null;

        $form = $this->createForm(FindOrderType::class);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $email = $form->get('email')->getData();
            $uuid = $form->get('uuid')->getData();

            $order = $orderRepository->findOneBy([
                'email' => $email,
                'uuid' => $uuid,
            ]);
        }

        return $this->render('front/find-order.html.twig', [
            'order' => $order,
            'form' => $form->createView(),
        ]);
    }
}
