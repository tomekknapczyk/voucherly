<?php

declare(strict_types=1);

namespace App\Controller\Front;

use App\Entity\Order;
use App\Event\OrderPaidEvent;
use App\Repository\OrderRepository;
use App\Service\PaymentService;
use Exception;
use Psr\Log\LoggerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class PaymentController extends AbstractController
{
    #[Route(path: '/payment-status', name: 'paymentStatus', methods: ['POST'])]
    public function paymentStatus(
        LoggerInterface $logger,
        Request $request,
        PaymentService $paymentService,
        OrderRepository $orderRepository,
        EventDispatcherInterface $dispatcher,
    ): Response {
        $signature = $request->headers->get('X-IMoje-Signature');
        $content = (string) $request->getContent();
        $parameters = json_decode($content, true, 512, JSON_THROW_ON_ERROR);

        try {
            $paymentService->verifySignature($content, $signature);

            $order = $orderRepository->findOneBy([
                'uuid' => $parameters['payment']['orderId'],
            ]);

            if ($order instanceof Order) {
                if ($paymentService->verifyPaymentStatus($parameters, 'settled')) {
                    $order->markAsPaid();
                    $orderRepository->add($order, true);

                    $event = new OrderPaidEvent($order);
                    $dispatcher->dispatch($event, OrderPaidEvent::NAME);

                    $logger->notice('Payment accepted', [
                        'paymentId' => $parameters['payment']['id'],
                    ]);
                }

                if ($paymentService->verifyPaymentStatus($parameters, 'rejected') || $paymentService->verifyPaymentStatus($parameters, 'cancelled')) {
                    $order->cancel();
                    $orderRepository->add($order, true);

                    $logger->notice('Payment canceled', [
                        'paymentId' => $parameters['payment']['id'],
                    ]);
                }
            } else {
                $logger->notice('Brak zamówienia z podanym id', [
                    'paymentId' => $parameters['payment']['id'],
                    'status' => $parameters['payment']['status'],
                ]);
            }
        } catch (Exception $e) {
            $logger->notice('Payment not accepted: ' . $e->getMessage(), [
                'paymentId' => $parameters['payment']['id'],
            ]);
        }

        return new JsonResponse([
            'status' => 'ok',
        ], Response::HTTP_OK);
    }

    #[Route(path: '/payment-return', name: 'paymentReturn')]
    public function paymentReturn(): Response
    {
        return $this->render('front/payment-return.html.twig');
    }
}
