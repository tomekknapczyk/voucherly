<?php

declare(strict_types=1);

namespace App\Controller\Front;

use App\Entity\Product;
use App\Form\AddToCartType;
use App\Service\CartService;
use App\Service\ReferenceService;
use Exception;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class ProductController extends AbstractController
{
    #[Route(path: '/product/{id}', name: 'product')]
    public function product(
        Product $product,
        Request $request,
        CartService $cartService,
        ReferenceService $referenceService
    ): Response {
        $form = $this->createForm(AddToCartType::class);

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $item = $form->getData();

            if ($item->isGift() && (!$item->getClientName() || !$item->getClientLastName())) {
                $this->addFlash('danger', 'Uzupełnij wszystkie wymagane pola');

                return $this->render('front/product.html.twig', [
                    'product' => $product,
                    'form' => $form->createView(),
                ]);
            }

            $item->setProduct($product);

            $cart = $cartService->getCurrentCart();
            try {
                $item->setUuid($referenceService->generateUniqReferenceForOrderItem());
            } catch (Exception $e) {
                $this->addFlash('error', 'Błąd podczas generowania indeksu vouchera. ' . $e->getMessage());

                return $this->redirectToRoute('product', [
                    'id' => $product->getId(),
                ]);
            }

            $cart->addItem($item);

            $cartService->save($cart);

            $this->addFlash('success', 'Produkt został dodany do koszyka');

            return $this->redirectToRoute('product', [
                'id' => $product->getId(),
            ]);
        }

        return $this->render('front/product.html.twig', [
            'product' => $product,
            'form' => $form->createView(),
        ]);
    }
}
