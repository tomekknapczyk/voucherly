# Voucherly

e-commerce platform for selling products as vouchers with custom templates

## Installation
### First run:
```bash
#setup docker environment
$ docker-compose up -d

# access web container
$ docker exec -it voucherly_web_1 bash

# install dependencies
root@:/var/www/voucherly# bin/console composer install 

#migrate database
root@:/var/www/voucherly# bin/console d:m:m 
```

### Build assets

```bash
$ npm install

# watch live changes of styles
$ npm run watch

# build production files
$ npm run prod

# tailwind build process with purge unused classes 
$ npx tailwindcss -i assets/styles/app.css -o public/build/app.css --watch
```
## License
MIT