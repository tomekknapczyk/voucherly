<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

final class Version20220830184046 extends AbstractMigration
{
    public function getDescription(): string
    {
        return 'Templates without text content but with image';
    }

    public function up(Schema $schema): void
    {
        $this->addSql('ALTER TABLE template ADD file_name VARCHAR(255) DEFAULT NULL, ADD file_size INT DEFAULT NULL, DROP content');
    }

    public function down(Schema $schema): void
    {
        $this->addSql('ALTER TABLE template ADD content LONGTEXT NOT NULL, DROP file_name, DROP file_size');
    }
}
