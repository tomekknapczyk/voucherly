<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

final class Version20220827202508 extends AbstractMigration
{
    public function getDescription(): string
    {
        return 'Add uuid to order and nr to order item';
    }

    public function up(Schema $schema): void
    {
        $this->addSql('ALTER TABLE `order` ADD uuid BINARY(16) NOT NULL COMMENT \'(DC2Type:uuid)\'');
        $this->addSql('ALTER TABLE order_item ADD uuid BINARY(16) NOT NULL COMMENT \'(DC2Type:uuid)\'');
    }

    public function down(Schema $schema): void
    {
        $this->addSql('ALTER TABLE `order` DROP uuid');
        $this->addSql('ALTER TABLE order_item DROP uuid');
    }
}
