<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

final class Version20220914165857 extends AbstractMigration
{
    public function getDescription(): string
    {
        return 'Add favicon for settings';
    }

    public function up(Schema $schema): void
    {
        $this->addSql('ALTER TABLE setting ADD favicon_name VARCHAR(255) DEFAULT NULL, ADD favicon_size INT DEFAULT NULL');
    }

    public function down(Schema $schema): void
    {
        $this->addSql('ALTER TABLE setting DROP favicon_name, DROP favicon_size');
    }
}
