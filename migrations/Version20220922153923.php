<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

final class Version20220922153923 extends AbstractMigration
{
    public function getDescription(): string
    {
        return 'Default voucher template';
    }

    public function up(Schema $schema): void
    {
        $this->addSql('ALTER TABLE setting ADD default_voucher_name VARCHAR(255) DEFAULT NULL, ADD default_voucher_size INT DEFAULT NULL');
    }

    public function down(Schema $schema): void
    {
        $this->addSql('ALTER TABLE setting DROP default_voucher_name, DROP default_voucher_size');
    }
}
