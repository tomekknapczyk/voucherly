<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

final class Version20220912182344 extends AbstractMigration
{
    public function getDescription(): string
    {
        return 'Product additional fields for icons';
    }

    public function up(Schema $schema): void
    {
        $this->addSql('ALTER TABLE product ADD place VARCHAR(255) DEFAULT NULL, ADD time VARCHAR(255) DEFAULT NULL, ADD peoples VARCHAR(255) DEFAULT NULL, ADD duration VARCHAR(255) DEFAULT NULL, ADD weight VARCHAR(255) DEFAULT NULL, ADD help_text VARCHAR(255) DEFAULT NULL');
    }

    public function down(Schema $schema): void
    {
        $this->addSql('ALTER TABLE product DROP place, DROP time, DROP peoples, DROP duration, DROP weight, DROP help_text');
    }
}
