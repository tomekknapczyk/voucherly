<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

final class Version20220824172755 extends AbstractMigration
{
    public function getDescription(): string
    {
        return 'Update Product fields';
    }

    public function up(Schema $schema): void
    {
        $this->addSql('ALTER TABLE product ADD short_description LONGTEXT DEFAULT NULL, CHANGE template_id template_id INT DEFAULT NULL, CHANGE voucher available_as_gift TINYINT(1) NOT NULL');
    }

    public function down(Schema $schema): void
    {
        $this->addSql('ALTER TABLE product DROP short_description, CHANGE template_id template_id INT NOT NULL, CHANGE available_as_gift voucher TINYINT(1) NOT NULL');
    }
}
