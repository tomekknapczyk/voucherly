<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

final class Version20220912164055 extends AbstractMigration
{
    public function getDescription(): string
    {
        return 'Order item use state';
    }

    public function up(Schema $schema): void
    {
        $this->addSql('ALTER TABLE order_item ADD used TINYINT(1) NOT NULL');
    }

    public function down(Schema $schema): void
    {
        $this->addSql('ALTER TABLE order_item DROP used');
    }
}
