<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

final class Version20220829201402 extends AbstractMigration
{
    public function getDescription(): string
    {
        return 'Order item status removed - use date is enough to determine if item was used';
    }

    public function up(Schema $schema): void
    {
        $this->addSql('ALTER TABLE order_item DROP status');
    }

    public function down(Schema $schema): void
    {
        $this->addSql('ALTER TABLE order_item ADD status VARCHAR(255) NOT NULL');
    }
}
