<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

final class Version20220929171645 extends AbstractMigration
{
    public function getDescription(): string
    {
        return 'Change uuid to client friendly string';
    }

    public function up(Schema $schema): void
    {
        $this->addSql('UPDATE `order` SET uuid = \'\'');
        $this->addSql('UPDATE `order_item` SET uuid = \'\'');
        $this->addSql('ALTER TABLE `order` CHANGE uuid uuid VARCHAR(255) DEFAULT NULL');
        $this->addSql('ALTER TABLE `order_item` CHANGE uuid uuid VARCHAR(255) DEFAULT NULL');
    }

    public function down(Schema $schema): void
    {
        $this->addSql('ALTER TABLE `order` CHANGE uuid uuid BINARY(16) NOT NULL COMMENT \'(DC2Type:uuid)\'');
        $this->addSql('ALTER TABLE `order_item` CHANGE uuid uuid BINARY(16) NOT NULL COMMENT \'(DC2Type:uuid)\'');
    }
}
