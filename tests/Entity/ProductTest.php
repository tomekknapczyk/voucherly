<?php

declare(strict_types=1);

namespace App\Tests\Entity;

use App\Entity\Product;
use DateTimeImmutable;
use PHPUnit\Framework\TestCase;

class ProductTest extends TestCase
{
    /**
     * @dataProvider priceProvider
     */
    public function testGetDisplayPrice(int $priceInPenny, float $roundedPrice): void
    {
        $product = new Product();
        $product->setPrice($priceInPenny); // price in penny

        $this->assertSame($roundedPrice, $product->getDisplayPrice()); // rounded price with 2 decimal places
    }

    /**
     * @dataProvider validityProvider
     */
    public function testValidThru(int $validity): void
    {
        $product = new Product();
        $product->setValidity($validity);

        if ($validity === 0) {
            $this->assertNull($product->validThru()?->getTimestamp());
        } else {
            $now = new DateTimeImmutable();
            $expectedValidDate = $now->modify('+ ' . $validity . ' days') ?: null;
            $this->assertSame($expectedValidDate?->getTimestamp(), $product->validThru()?->getTimestamp());
        }
    }

    /**
     * @return  array<int, array<int, float>>
     */
    public function priceProvider(): array
    {
        return [
            [1470, 14.70],
            [39, 0.39],
            [15065, 150.65],
            [1479145, 14791.45],
        ];
    }

    /**
     * @return  array<int, array<int, int>>
     */
    public function validityProvider(): array
    {
        return [
            [10],
            [0],
            [150],
            [1490],
        ];
    }
}
