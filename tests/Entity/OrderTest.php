<?php

declare(strict_types=1);

namespace App\Tests\Entity;

use App\Entity\Order;
use App\Entity\OrderItem;
use App\Entity\Product;
use PHPUnit\Framework\TestCase;

class OrderTest extends TestCase
{
    protected Product $product;

    protected function setUp(): void
    {
        $this->product = new Product();
        $this->product->setPrice(1470); // price in penny
    }

    public function testMarkAsPaid(): Order
    {
        $order = new Order();
        $this->assertSame(Order::STATUS_CART, $order->getStatus());
        $order->markAsPaid();
        $this->assertSame(Order::STATUS_PAID, $order->getStatus());

        return $order;
    }

    /**
     * @depends testMarkAsPaid
     */
    public function testCancel(Order $order): Order
    {
        $order->cancel();
        $this->assertSame(Order::STATUS_CANCELED, $order->getStatus());

        return $order;
    }

    /**
     * @depends testMarkAsPaid
     */
    public function testGetTotalItem(Order $order): Order
    {
        $orderItem = new OrderItem();
        $orderItem->setProduct($this->product);

        $orderItem2 = new OrderItem();
        $orderItem2->setProduct($this->product);

        $order->addItem($orderItem);
        $order->addItem($orderItem2);
        $this->assertSame(2, $order->getTotalItems());

        $order->removeItem($orderItem2);
        $this->assertSame(1, $order->getTotalItems());

        return $order;
    }

    /**
     * @depends testGetTotalItem
     */
    public function testGetTotalPrice(Order $order): void
    {
        $this->assertSame(14.70, $order->getTotalPrice()); // rounded price with 2 decimal places
    }
}
